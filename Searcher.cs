﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Windows_Search
{
    class Searcher
    {

        public static void search(string filename, string path, ListBox resuListBox, ProgressBar progressBar, int progress = 1)
        {
            if (path == "")
            {
                var allDrives = getAllDrives();
                progress = progress + allDrives.Length;
                foreach (var drive in allDrives)
                {
                    Task.Run(() => search(filename, drive.Name, resuListBox, progressBar, progress));
                }
            }
            else
            {
                var file = Task.Run((() => getFile(path, filename)));
                var directories = getDirectories(path);
                if (directories != null)
                {
                    progress = progress + directories.Length;
                    foreach (var directory in directories)
                    {
                        if (directory.Contains(filename))
                        {
                            resuListBox.Dispatcher.Invoke(() =>
                                {
                                    if (file.Result is string)
                                    {
                                        resuListBox.Items.Add(directory);
                                    }
                                }
                            );
                        }
                        file = Task.Run((() => getFile(path, filename)));
                        search(filename, directory, resuListBox, progressBar, progress);
                    }
                }

                progressBar.Dispatcher.Invoke(() =>
                {
                    progressBar.Value = progress;
                });
                resuListBox.Dispatcher.Invoke(() =>
                    {
                        if (file.Result is string)
                        {
                            resuListBox.Items.Add(file.Result);
                        }
                    }
                );
            }
        }

        private static DriveInfo[] getAllDrives()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            return allDrives;
        }

        private static string getFile(string path, string filename)
        {
            List<string> allFiles = new List<string>();
            try
            {
                var files = Directory.GetFiles(path);
                allFiles = files.ToList();
            }
            catch (Exception e)
            {
                return null;
            } 
            foreach (var file in allFiles)
            {
                if (file.Contains(filename))
                {
                    return file;
                }
            }
            return null;
        }

        private static string[] getDirectories(string path)
        {
            try
            {
                string[] allDirectories = Directory.GetDirectories(path);
                return allDirectories;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }



    }
}
