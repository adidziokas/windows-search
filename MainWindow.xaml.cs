﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windows_Search
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Searcher.search(fileTextBox.Text, folderTextBox.Text, resultList, progressBar);
            var lol = 1;
            // searcher.search(fileTextBox.Text,);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            folderTextBox.IsEnabled = true;
        }

        private void folderCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            folderTextBox.Text = "";
            folderTextBox.IsEnabled = false;
        }
    }
}
